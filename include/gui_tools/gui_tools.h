#ifndef GUI_TOOLS__GUI_TOOLS__H
#define GUI_TOOLS__GUI_TOOLS__H

#include <memory>
#include <string>
#include <map>
#include <GLFW/glfw3.h>
#include "imgui.h"
#include "implot.h"
#include "imgui_stdlib.h"

namespace gui_tools
{
    namespace button
    {
        static const int LEFT = 0;
        static const int RIGHT = 1;
        static const int MIDDLE = 2;
    }

    struct App
    {
        /// @brief Constructor - initialize GLFW and ImGui, create main window
        App(const std::string &title, int width, int height, bool vsync=true, bool use_msaa=false);
        /// @brief Destructor - cleanup everything
        ~App();
        /// @brief Check if window should be closed
        inline bool windowShoudClose() {return glfwWindowShouldClose(window_);}
        /// @brief Process events and initialize new imgui frame
        void beginFrame();
        /// @brief Clear window and process rendering
        void endFrame();
        /// @brief Configure the main window background color from ImVec4
        void setBackgroundColor(ImVec4 color);
        /// @brief Configure the main window background color from float[4]
        void setBackgroundColor(float color[]);
        /// @brief Set icon
        void setIcon(int width, int height, unsigned char *data);
        /// @brief Get window size
        ImVec2 getWindowSize() const;

    private:
        /// @brief  Background clear color
        ImVec4 clear_color_={0.45f, 0.55f, 0.60f, 1.00f};
        /// @brief  GLFW window handler
        GLFWwindow* window_;
        /// @brief  Font map
        std::map<std::string,ImFont*> fonts_;
        /// @brief  Application icon
        GLFWimage image;
    };


    struct Image
    {
        GLuint texture_id;
        ImVec2 size;

        Image()
        {
            glGenTextures(1, &texture_id);
            size.x = -1;
            size.y = -1;
        }

        ~Image()
        {
            glDeleteTextures(1, &texture_id);
        }

        inline void *toImGui()
        {
            return (void*)(intptr_t)texture_id;
        }

        inline void fromMemoryRGBA(int w, int h, uint8_t *data)
        {
            // Internal OpenGL - bind a named texture to a texturing target
            glBindTexture(GL_TEXTURE_2D, texture_id);
            // Setup filtering parameters for display
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // This is required on WebGL for non power-of-two textures
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // Same
            // Upload pixels into texture
            #if defined(GL_UNPACK_ROW_LENGTH) && !defined(__EMSCRIPTEN__)
            glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
            #endif
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
            size.x = static_cast<float>(w);
            size.y = static_cast<float>(h);
        }

        bool fromFile(const std::string& filename);
    };
}


std::ostream &operator<<(std::ostream &os, const ImVec2 &value);

#endif
