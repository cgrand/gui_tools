# gui_tools

## Description

C++ library used to create application based on Immediate GUI paradigm (ImGUI)

Package includes third-party sources:

- Dear ImGui (https://github.com/ocornut/imgui.git)
- ImPlot (https://github.com/epezent/implot)

and is based on GLFW library.

This library simplifies the creation of:

- Main windows,
- OpenGL context,
- management of gui event

## Dependencies

    sudo apt install libglfw3-dev

## Install

~~~
cd ${ROS2_WS}/src
git clone --recurse-submodules git@gitlab.com:cgrand/gui_tools.git
cd ..
colcon build --packages-select gui_tools
~~~

